<!DOCTYPE html>
<html lang="en">
<?php include'includes/head.php'; ?>
  <body class="loading">
    <div class="kloudboy">
      <?php include 'includes/header.php'; ?>      
      <?php include 'content/engineering.php';?>    
      <?php include 'nav/engineering.php';?>
      <?php include 'includes/footer.php';?>
    </div>
    <?php include 'includes/scripts.php'?>
  </body>
</html>