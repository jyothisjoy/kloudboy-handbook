<section class="section">
<div class="container wow fadeIn">
<div class="row">
   <div class="col-lg-4">
      <div class="article-date">0.0.1</div>
   </div>
   <div class="col-lg-8">
      <div class="section-title mb-40">
         <h1>Engineering</h1>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">KloudBoy</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">Engineering</a></li>
        </ol>
      </nav>
   </div>
</div>
<div class="row justify-content-between">
<div class="col-lg-8 order-lg-2">
   <div class="article-full">
      <h3 id="introduction">Introduction</h3>
      <p><b>Mission:</b>  KloudBoy’s engineering team should strive to code, design & deliver any client projects or product requirement. The team should ensure transparency and should prioritize the client issues first. The team should also work in the most efficient manner possible, at KloudBoy it’s all about working smartly.</p>
      <p><b>Vision:</b> To create the most efficient, high performance servers & websites for the customers.</p>
      
      <span class="spacer"></span>
      
      <h3 id="sdd">Service Development Department</h3>
      <p>Purpose:  Purpose of Service Department will be to deliver high performance servers and websites to the customer. Issues that arise from the customer should be dealt in collaboration with respective designers, managers and UI/UX developers. All team members in the service development department is expected to follow the voice of the company</p>

      <span class="spacer"></span>
      
      <h3 id="sdd">Product Development Department</h3>
      <p>Purpose:  Product Development Department is to create high-performance service and deploy it as a SAAS application. Department will always try to create the most efficient software possible to make KloudBoy the most leading product in the niche.</p>


      <span class="spacer"></span>
      
      <h3 id="id">Infrastructure Department</h3>
      <p>Purpose: Purpose of infrastructure department East support the product department with the necessary infrastructure like Service communication with its partners et cetera</p>

      <span class="spacer"></span>
      
      <h3 id="sd">Security Department</h3>
      <p>Purpose: Security department will be in charge of the entire security of the product infrastructure and client sites.</p>

      <span class="spacer"></span>
      
      <h3 id="sud">Support Department</h3>
      <p>Purpose: KloudBoy support team will help the client in difficult situations. Support team will answer the tickets raised in the support website. They will communicate with the necessary departments to provide the best support they can for the client</p>
   </div>
</div>