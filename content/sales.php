<section class="section">
<div class="container wow fadeIn">
<div class="row">
   <div class="col-lg-4">
      <div class="article-date">0.0.1</div>
   </div>
   <div class="col-lg-8">
      <div class="section-title mb-40">
         <h1>Sales</h1>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">KloudBoy</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">Sales</a></li>
        </ol>
      </nav>
   </div>
</div>
<div class="row justify-content-between">
<div class="col-lg-8 order-lg-2">
   <div class="article-full">
      <h3 id="introduction">Introduction</h3>
   </div>
</div>