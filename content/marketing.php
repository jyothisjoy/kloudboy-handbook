<section class="section">
<div class="container wow fadeIn">
<div class="row">
   <div class="col-lg-4">
      <div class="article-date">0.0.1</div>
   </div>
   <div class="col-lg-8">
      <div class="section-title mb-40">
         <h1>Marketing</h1>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">KloudBoy</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">Marketing</a></li>
        </ol>
      </nav>
   </div>
</div>
<div class="row justify-content-between">
<div class="col-lg-8 order-lg-2">
   <div class="article-full">
		<h3 id="introduction">Introduction</h3>
		<p>Intro</p>

		<span class="spacer"></span>

		<h3 id="wb">Website & Blog</h3>
		<p><h6>Objective</h6>
	      	<ol>
	      		<li>Build Authority and Credibility</li>
	      		<li>Increase the brand awareness among the mass</li>
	      		<li>Organic traffic into the site</li>
	      		<li>Lead Generation</li>
	      	</ol>
		</p>
		<span class="spacer"></span>
		<p>
			<h6>Guides</h6>
			<ul>
				<li>All the contents are should go in the blocks will be decided by the content strategist. Content strategist is also responsible for creating and updating content calendar.</li>
				<li>Progress in respective of blog should be discussed in Bi weekly and monthly meetings to be held by the marketing team.</li>
				<li>Content developers should consult with respective members for any technical details necessary to complete the blog posts.</li>
				<li>KloudBoy’s focus shouldn't be to write the maximum number of blog posts in a week but to create best possible and authoritative article on the subject.</li>
			</ul>	
		</p>
      	
      	<span class="spacer"></span>
      	<span class="spacer"></span>
      	
      	<h3 id="smg">Social Media Guidelines</h3>
      	<h6>Guides</h6>
      	<ul>
      		<li>This document is for people who deal with social media accounts of Kloud boy.</li>
      		<li>Members who post in respective social media handles will be personally responsible for the content that is published.</li>
      		<li>Arguing is not recommended for commenting on any post.</li>
      	</ul>
      	<h6>Responding To Mentions</h6>
      	<ol>
      		<li>Respond quickly</li>
 			<li>Everyone is expected to participate, if they are on the social media team or leadership team. Every member of KloudBoy represents the company.</li>
 			<li>Respond in the most personal way possible. This doesn't mean that it you should get very unprofessional with the clients. It means that you should treat them as you treat any other human.</li>
 			<li>For the same reason we will not be using any bots in the social media handles.</li>
      	</ol>

      	<span class="spacer"></span>
      	
      	<h3 id="kv">KloudBoy Voice</h3>
      	<p>While communicating in any media or platform the  ‘KloudBoy’ voice.</p>
      	<h6>Guides</h6>
      	<ul>
      		<li>Always use ‘We’  instead of ‘I’.</li>
      		<li>Be Responsive, Open Minded, Confident & Caring in the social media platforms.</li>
      		<li>Criticism should be treated as an opportunity to improve the service and the product.</li>
      		<li>If someone have an idea to make the service or the product better invite them for a future proposal. </li>
      		<li>Do not engage in Competitor bashing</li>
      	</ul>

      	<h3 id="dm">Digital Marketing</h3>
   </div>
</div>