<section class="section">
<div class="container wow fadeIn">
<div class="row">
   <div class="col-lg-4">
      <div class="article-date">0.0.1</div>
   </div>
   <div class="col-lg-8">
      <div class="section-title mb-40">
         <h1>General</h1>
      </div>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">KloudBoy</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">General</a></li>
        </ol>
      </nav>
   </div>
</div>
<div class="row justify-content-between">
<div class="col-lg-8 order-lg-2">
   <div class="article-full">
      <h3 id="introduction">Introduction</h3>
      <p>HandBook is the bible with which KloudBoy works.  In-depth company culture will be defined in the HandBook. Any employee can add points to the HandBook.</p>
      
      <span class="spacer"></span>
      
      <h3 id="values">Values</h3>
      <p>
        <ul>
          <li><b>Collaboration:</b> KloudBoy is what it is today with the help of the community. Anyone working at KloudBoy is expected to carry on with the same attitude towards anyone. Helping people is the number one priority.</li>
          <li><b>Efficiency:</b> We believe in working smart. Also, anything that’s working doesn’t have to be fixed. Productivity is given the utmost importance, not the number of hours you sit in-front of the workstation.</li>
          <li><b>Inclusion:</b> We care for our employees to have similar values not similar culture. We are a remote company and inclusion of any culture is a primary motive of KloudBoy. We try to promote a comfortable culture for all, created by all.</li>
          <li><b>Transparency :</b> Everything that we do are public by default. KloudBoy is transparent, open source first company. Almost everything is public facing except some obvious data like revenue, proprietary algorithms, partnership contracts, any data that would infringe on employee privacy, legal discussions etc...</li>
        </ul>
      </p>
   </div>
</div>