<head>
    <title>KloudBoy | HandBook</title>
    <meta charset="utf-8">
    <meta name="description" content="KloudBoy is a High-Performance, Managed WordPress Hosting service provide. We also provide WordPress Development and Optimization services for quality control.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="512x512" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="shortcut icon" href="favicon/favicon-32x32.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/main.css">
    <link href="https://www.kloudboy.com/wp-content/uploads/useanyfont/uaf.css" rel="stylesheet">
  </head>