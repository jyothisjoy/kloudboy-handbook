
      <!-- footer-->
      <footer class="footer">
        <div class="footer__inner">
          <div class="container wow fadeIn">
            <div class="row">
              <div class="col-md-3 col-6 mb-40">
                <h4>About Us</h4>
                <p>KloudBoy is a High-Performance, Managed WordPress Hosting service provide. We also provide WordPress Development and Optimization services for quality control.</p>
              </div>
              <div class="col-md-3 col-6 mb-40">
                <h4>Company</h4>
                <ul>
                  <li><a href="https://kloudboy.com">Home</a></li>
                  <li><a href="https://www.kloudboy.com/about-us/">About Us</a></li>
                  <li><a href="https://www.kloudboy.com/about-us/leadership/">Leadership</a></li>
                  <li><a href="https://www.kloudboy.com/about-us/press/">Press</a></li>
                  <li><a href="https://www.kloudboy.com/careers/">Careers</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6 mb-40">
                <h4>Navigate</h4>
                <ul>
                  <li><a href="https://www.kloudboy.com/services-pricing/">Services & Pricing</a></li>
                  <li><a href="https://www.kloudboy.com/blog/">Blog</a></li>
                  <li><a href="https://www.kloudboy.com/request-a-quote/">Request a Quote</a></li>
                  <li><a href="https://www.kloudboy.com/contact/">Contact</a></li>
                  <li><a href="https://www.kloudboy.com/privacy-policy/">Privacy Policy</a></li>
                </ul>
              </div>
              <div class="col-md-3 col-6 mb-40">
                <h4>Office Address</h4>
                <p>
                   9 Cotton Field Way, Narre Warren South, 3805, VIC, Australia
                   <br><br>
                    Phone: +61 4149 56598
                    <br><br>
                    Email: info@kloudboy.com</p>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright">
          <div class="container">
            <div class="row">
              Address:<br>
              Kunnumpuram, Desom(PO), Kochi, Kerala - 683102
              <br><br>
              KLOUDBOY - 2019
            </div>
          </div>
        </div>
      </footer>
      <!-- ./ footer-->